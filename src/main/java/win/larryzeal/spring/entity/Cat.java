package win.larryzeal.spring.entity;

/**
 * Created by 张少昆 on 2017/9/14.
 */
public class Cat {
	private String id;
	private String name;

	public String getId(){
		return id;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getName(){
		return name;
	}

	public void setName(String name){
		this.name = name;
	}

	@Override
	public String toString(){
		return "Cat{" +
				"id=" + id +
				", name='" + name + '\'' +
				'}';
	}
}

package win.larryzeal.spring.entity;

/**
 * Created by 张少昆 on 2017/9/14.
 */
public class Book {
	private long id;
	private String name;
	private String version;

	public long getId(){
		return id;
	}

	public void setId(long id){
		this.id = id;
	}

	public String getName(){
		return name;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getVersion(){
		return version;
	}

	public void setVersion(String version){
		this.version = version;
	}

	@Override
	public String toString(){
		return "Book{" +
				"id=" + id +
				", name='" + name + '\'' +
				", version='" + version + '\'' +
				'}';
	}
}

package win.larryzeal.spring.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;

/**
 * Created by 张少昆 on 2017/9/14.
 */
@Configuration
@PropertySource( value = "application.properties", ignoreResourceNotFound = true )
@ComponentScan( basePackages = "win.larryzeal.spring", excludeFilters = @ComponentScan.Filter(type = FilterType.ANNOTATION,classes = Controller.class ) )
public class AppConfig {

}

package win.larryzeal.spring.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;

import java.time.Instant;


/**
 * 当接收 conditional GET/HEAD requests时， checkNotModified 会检查resource是否没有被修改；
 * 如果没有，它会返回一个HTTP 304 Not Modified response。
 * <p>
 * 而在POST/PUT/DELETE requests时，checkNotModified 会检查resouce是否没有被修改；
 * 如果有修改，会返回一个HTTP 409 Precondition Failed response 来阻止并发修改。
 * <p>
 * Created by 张少昆 on 2017/9/14.
 */
@Controller
@RequestMapping( "/cat" )
public class CatController {
	/**
	 * 本方法是不使用ResponseEntity，自行处理。
	 * 注意参数！WebRequest的check方法会自动返回响应 -- 如果匹配了！
	 * tip：这样的话，其实也可以获取@RequestHeader，自行判断，再做出响应。
	 * <p>
	 * ps: 这里是页面整体是否更新，还可以是其他的，只要是请求即可。
	 *
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping( "/1" )
	public String test(WebRequest request, Model model){

		// 1. 应该从业务逻辑中获取（应该放在哪里？？？）
		long lastModified = getLastModified();

		// 2. 检查是否已更改，如果没有更改，直接返回
		if(request.checkNotModified(lastModified)){ //the Last-Modified header will contain the date of last modification.
			return null; // 关键！让Spring MVC 不再进一步处理。因为checkNotModified会直接返回304
		}

		// 3. 如果有了更改，那返回新的内容
		model.addAttribute("test1", "cat test"); // 返回的jsp中可以直接使用${key}获取到值。FIXME 不建议使用默认name！
		model.addAttribute("test2", "hehe1");

//		ModelAndView mv = new ModelAndView("what"); // 对应的是还是view name，viewResolver使用的。

		return null; //成了当前请求了 ( /WEB-INF/views/cat/1.jsp )
	}

	private int counter = 0;
	private long lastModified = Instant.now().toEpochMilli();

	/**
	 * 模拟获取LastModified值
	 *
	 * @return
	 */
	private long getLastModified(){
		if((++counter) % 5 == 0){ //请求满5次再更改eTag。
			lastModified += 1000; // 囧，这个值只能是时间，而且，最小匹配时间是【秒】，不能是毫秒
		}
		return lastModified;
	}

}
